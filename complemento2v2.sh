#!/bin/bash

l1=$(wc -l <$1)
l2=$(wc -l <$2)
l3=$(wc -l <$3)
l4=$(wc -l <$4)
if [[ $l1 -gt $l2 ]];then
	if [[ $l1 -gt $l3 ]];then
		if [[ $l1 -gt $l4 ]];then
			echo $(cat $1)
		fi
	fi
fi

if [[ $l2 -gt $l1 ]];then
	if [[ $l2 -gt $l3 ]];then
		if [[ $l2 -gt $l4 ]];then
			echo $(cat $2)
		fi
	fi
fi

if [[ $l3 -gt $l1 ]];then
	if [[ $l3 -gt $l2 ]];then
		if [[ $l3 -gt $l4 ]];then
			echo $(cat $3)
		fi
	fi
fi

if [[ $l4 -gt $l1 ]];then
	if [[ $l4 -gt $l2 ]];then
		if [[ $l4 -gt $l3 ]];then
			echo $(cat $4)
		fi
	fi
fi
